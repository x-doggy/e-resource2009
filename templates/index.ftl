<#import "_partials/page.ftl" as page>

<@page.page>
	<h1 class="c-title-header">Электронный учебник по моделированию</h1>
	<ol>	
	<#list all_content?sort_by("date") as post>
		<#if (post.status == "published")>
			<li><a href="${post.uri}"><#escape x as x?xml>${post.title}</#escape></li>
		</#if>
	</#list>
	</ol>
</@page.page>