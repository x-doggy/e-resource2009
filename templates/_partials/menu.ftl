<div class="accordeon"> 
  <div class="tab">
    <input type="checkbox" id="tab1" name="tab-group">
    <label for="tab1" class="tab-title pseudo-link">Показать настройки</label> 
    <section class="tab-content"> 
      <div class="tab-content-layout">
        <div class="tab-content-layout__part">
          <form>
            <div>Перейти:</div>
            <select name="page" id="page-selection" class="page-selection-interactive">
            <option value="${content.rootpath}index.html">Главная</option>
            <#list all_content?sort_by("date") as _content>
              <option value="${_content.rootpath}${_content.uri}" <#if content.title == _content.title>selected</#if>><#escape x as x?xml>${_content?counter}. ${_content.title}</#escape></option>
            </#list>
            </select>
          </form>
        </div>
        <div class="tab-content-layout__part tab-content-layout__part--second">
          <span class="clock-interactive" id="clock-interactive">--:--:--</span>
        </div>
      </div>
    </section>
  </div> 
</div>
