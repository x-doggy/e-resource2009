<#include "menu.ftl">

<main class="c-article-body" id="_body">
  <h5 class="c-moduletype"><#if content.type == "post">Информационны<#else>Практически</#if>й модуль</h5>
  <h2 class="c-title-header"><#escape x as x?xml>${content.title}</#escape></h2>
	${content.body}
</main>

<footer class="c-footer-nav-links">
  <#assign prevNum = 0 />
  <#assign cnt = all_content?sort_by("date") />
  <#list cnt as c>
    <#if c.uri == content.uri><#break><#else><#assign prevNum++ /></#if>
  </#list>
  <#assign nextNum = prevNum + 1 />
  <#if prevNum gt 0><#assign prevNum-- /></#if>
  <#assign indexStr = "index.html" />

  <#if (cnt[prevNum])?? || (cnt[nextNum])??>
    <#if (cnt[prevNum])??>
      <#if prevNum == 0>
        <#assign prevLinkPart = indexStr />
      <#else>
        <#assign prevLinkPart = cnt[prevNum].uri />
      </#if>
      <a href="<#if (content.rootpath)??>${content.rootpath}<#else></#if>${prevLinkPart}">назад</a>
    </#if>
    <#if (cnt[prevNum])?? && ((cnt[nextNum])?? || nextNum == cnt?size)> | </#if>
    <#if (cnt[nextNum])??>
      <#assign nextLinkPart = cnt[nextNum].uri />
      <#assign nextLinkText = "далее" />
    <#elseif nextNum == cnt?size>
      <#assign nextLinkPart = indexStr />
      <#assign nextLinkText = "на главную" />
    </#if>
    <a href="<#if (content.rootpath)??>${content.rootpath}<#else></#if>${nextLinkPart}">${nextLinkText}</a>
  </#if>
</footer>
