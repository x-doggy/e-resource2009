<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">

<#macro dumpUrl url>
<ul>
  <li>
    <span>${url?keep_before("/")}</span>
    <#if (url?keep_after("/")?length) != 0>
    <@dumpUrl url?keep_after("/") />
    <#else>
  </li>
</ul>
    <#return>
    </#if>
  </li>
</ul>
</#macro>

<published_content>
<#list published_content as content>
    <#assign uri = "${config.site_host}/${content.uri}" />
    <url>
        <loc>${uri}</loc>
        <lastmod>${content.date?string("yyyy-MM-dd")}</lastmod>
    </url>
</#list>
    <dump>
    <@dumpUrl uri />
    </dump>
</published_content>
</urlset>
