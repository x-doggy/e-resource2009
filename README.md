# Digital information modeling teachbook

My project of 2009.

You can check [this](https://x-doggy.gitlab.io/e-resource2009) out!

This project is built using [JBake](https://jbake.org) - a Java-based static site generator.

![Screenshot](readme-src/e-resource-fullpage.png)

## Usage

Running `jbake` without arguments is equivalent to running `jbake` with `-h`, and will display the usage instructions.

```sh
jbake
```

### Bake command

The bake command mixes your source content with your templates and cooks it all into a fully baked web site. You have a number of different ways to execute the bake command to suite your requirements.

The `-b` option without any parameters will assume your source content is in the current working directory and will place the baked output into a directory named output in the current directory:

```sh
jbake -b
```

> JBake will create this output directory if it does not already exist.

If you specify only the source directory as a parameter then the output directory will automatically be generated inside the source directory:

```sh
jbake -b <source>
```

You can also specify your source directory and output directory to enable you to have full control over the baking process:

```sh
jbake -b <source> <output>
```

You can even omit the `-b` option if you supply the source directory and output directory parameters:

```sh
jbake <source> <output>
```
